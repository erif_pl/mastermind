# Mastermind - classic edition

## Score analysis
Number of mispalced pegs: total number of similar pegs between code and guess minus pegs already on right spot.

![Score analysis](doc/score_analysis.jpg)

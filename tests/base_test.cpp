#include <gtest/gtest.h>

#include "../src/mastermind.c"

TEST(BaseTest, TestEverything) {
  EXPECT_EQ(42, everything());
}

// Start with exposed data structure for static mem allocation
//
// Constraints:
// Classical game (4 holes, 6 colors)
//
// Interfaces:
// game_start(game) - inits game
// score = game_guess(game, code) - score single guess

TEST(GameSetup, GameHasCodeToGuess) {
    game_state_t game;
    init_game(&game);

    for( int i=0; i<N_HOLES; i++) {
        EXPECT_GE(game.code[i], 0);
        EXPECT_LE(game.code[i], N_COLORS);
    }
}

TEST(ScoreLine, NoRightAns) {
    game_state_t game {.code = {0 ,0, 0, 0}};
    int guess[4] = {1, 1, 1, 1};

    game_score_t score = score_line(&game, guess);
    EXPECT_EQ(0, score.hit);
}

TEST(ScoreLine, AllRightAns) {
    game_state_t game {.code = {1 ,1, 1, 1}};
    int guess[4] = {1, 1, 1, 1};

    game_score_t score = score_line(&game, guess);
    EXPECT_EQ(4, score.hit);
}

TEST(ScoreLine, RightAnsAtSides) {
    game_state_t game {.code = {3 ,0, 2, 3}};
    int guess[4] = {3, 2, 0, 3};

    game_score_t score = score_line(&game, guess);
    EXPECT_EQ(2, score.hit);
}

TEST(ScoreLine, NoMisplacedAns) {
    game_state_t game {.code = {0 ,1, 2, 3}};
    int guess[4] = {0, 1, 2, 3};

    game_score_t score = score_line(&game, guess);
    EXPECT_EQ(0, score.miss);
}

TEST(ScoreLine, AllMisplacedAns) {
    game_state_t game {.code = {0 ,1, 2, 3}};
    int guess[4] = {3, 0, 1, 2};

    game_score_t score = score_line(&game, guess);
    EXPECT_EQ(4, score.miss);
}

TEST(ScoreLine, MisplacedAnsAtSides) {
    game_state_t game {.code = {0 ,1, 2, 3}};
    int guess[4] = {3, 1, 2, 0};

    game_score_t score = score_line(&game, guess);
    EXPECT_EQ(2, score.miss);
}

TEST(ScoreLine, MisplacedAndHitAns) {
    game_state_t game {.code = {0 ,1, 2, 3}};
    int guess[4] = {0, 1, 3, 2};

    game_score_t score = score_line(&game, guess);
    EXPECT_EQ(2, score.hit);
    EXPECT_EQ(2, score.miss);
}


TEST(ScoreLine, MultipleMisplacedInGuessAns) {
    game_state_t game {.code = {1 ,2, 3, 4}};
    int guess[4] = {0, 1, 1, 1};

    game_score_t score = score_line(&game, guess);
    EXPECT_EQ(1, score.miss);
}


TEST(ScoreLine, MultipleMisplacedInCodeAns) {
    game_state_t game {.code = {1 ,1, 1, 4}};
    int guess[4] = {0, 0, 0, 1};

    game_score_t score = score_line(&game, guess);
    EXPECT_EQ(1, score.miss);
}

TEST(ScoreLine, TwoMisplacedAns) {
    game_state_t game {.code = {1 ,1, 4, 4}};
    int guess[4] = {0, 0, 1, 1};

    game_score_t score = score_line(&game, guess);
    EXPECT_EQ(2, score.miss);
}

TEST(ScoreLine, HitAndMissOnSameColourAns) {
    game_state_t game {.code = {1 ,1, 4, 4}};
    int guess[4] = {0, 1, 1, 1};

    game_score_t score = score_line(&game, guess);
    EXPECT_EQ(1, score.hit);
    EXPECT_EQ(1, score.miss);
}
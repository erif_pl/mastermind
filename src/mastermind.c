#include <stdlib.h>

#include "mastermind.h"

int everything(void) {
    return 42;
}

void init_game(game_state_t *game) {
    for( int i=0; i<N_HOLES; i++) {
        // Good enough for simple game as N_COLORS << INT_MAX
        game->code[i] = rand() % N_COLORS;
    }
}

game_score_t score_line(game_state_t *game, const int *guess) {
    
    game_score_t result = {.hit=0, .miss = 0};

    // Need to count colors in both code and guess
    int code_cnt[N_COLORS] = {0};
    int guess_cnt[N_COLORS] = {0};

    for( int i=0; i<N_HOLES; i++) {
        if( game->code[i] == guess[i] ) {
            result.hit++;
        }
        else {
            code_cnt[game->code[i]]++;
            guess_cnt[guess[i]]++;
        }
    }

    // Now pick smaller count value for each color
    for( int i=0; i<N_COLORS; i++) {
        result.miss += (code_cnt[i] < guess_cnt[i]) ? code_cnt[i] : guess_cnt[i];
    }

    return result;
}

#ifndef _MASTERMIND_H_
#define _MASTERMIND_H_

#define N_HOLES  4
#define N_COLORS 6

struct game_state_s {
    int code[N_HOLES];
};
typedef struct game_state_s game_state_t;

struct game_score_s {
    int hit;
    int miss;
};
typedef struct game_score_s game_score_t;

int everything(void);
void init_game(game_state_t *game);
game_score_t score_line(game_state_t *game, const int *guess);

#endif